#    Copyright (C) 2023 U., J. Borge, J. Jornet-Somoza, N. Sobrino, G. Sobrino

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import numpy as np
from qiskit import QuantumCircuit
import matplotlib.pyplot as plt

class ibm_error():
    def __init__(self):
        self.qubit_list = []
        self.cx = None
        self.sgate = None
        self.time = None
        self.measurement = None
        self.total = None
        self.total_qubits = []  # New attribute for storing per-qubit errors

def qubit_time_error(time, t1, t2):
    """Compute the approx. idle error from T1 and T2."""
    t2 = min(t1, t2)
    rate1 = 1/t1
    rate2 = 1/t2
    p_reset = 1 - np.exp(-time * rate1)
    p_z = (1 - p_reset) * (1 - np.exp(-time * (rate2 - rate1))) / 2
    return p_z + p_reset



def ibm_circuit_error(qc, backend):
    '''Computes the total error probability of a quantum circuit and the time needed to execute it.'''
    gate_list = []
    fidel_cnot, fidel_sgate, fidel_measurement, fidel_time = 1, 1, 1, 1
    error = ibm_error()
    #props = backend.properties()
    #print('Getting backend properties')
    gatetimes = backend.instruction_durations
    #t1s = [backend.qubit_properties(qq).t1 for qq in range(qc.num_qubits)]
    #t2s = [backend.qubit_properties(qq).t2 for qq in range(qc.num_qubits)]
    error.qubit_list = sorted(qc.layout.final_index_layout())
    time = [0 for _ in error.qubit_list]
    fidel_qb = [1 for _ in error.qubit_list] 
    fidel_gate = [1 for _ in error.qubit_list] 
    fidel_time = [1 for _ in error.qubit_list] 
    
    gatememory = [[0] * len(qc._data) for _ in range(len(qc.layout.final_index_layout()))]
    gateerror = [0] * len(qc._data)

    # store t1 and t2 of qubit_list
    tdecay = [ [backend.qubit_properties(qubit).t1, backend.qubit_properties(qubit).t2] for qubit in error.qubit_list ] 

    # Check for 2qubits errors, and WARNING if any is 30% larger than the main value. 
    # TODO: check if wiht backend.properties().faulty_gates() it is enough
    # 1. check for the 2q gate in the backend 
    cnot_names = ['cx', 'ecr', 'cz']
    cnot_in_backend = next((op for op in cnot_names if op in backend.operation_names), None)
    #print(' 2qubit gates in backend {} : {}'.format(backend.name, cnot_in_backend))
    # 2. Obtained gate properties for CNOT gaate
    cnot_properties = backend.properties().gate_property(cnot_in_backend)
    # 3. list qubits paired with cnots
    qubitpairs = [ (gate.qubits[0]._index, gate.qubits[1]._index) for gate in qc._data if gate.name == cnot_in_backend ]
    # 4. : reduce to unique gates, and check if they are present into cnot_properties.keys(), else revert.
    #qubitpairs = list(set(qubitpairs))
    qubitpairs = list({tuple(sorted(pair)) for pair in qubitpairs})
    #print('qubitpairs= {}'.format(qubitpairs))
    # 5. For HERON processors the gate_properties is "randomly" saved. 
    #    Find for pair in gate_properties()...if not, invert qubit labels.
    for i, pair in enumerate(qubitpairs):
        if pair not in cnot_properties.keys():
            #print(pair, pair[::-1])
            qubitpairs[i]=pair[::-1]

    #cnot_gates = [ gate in qct._data if gate.name == cnot_in_backend ]
    #qubitpairs = [ (gate.qubits[0]._index, gate.qubits[1]._index) for gate in cnot_gates ]
    # 3. Compute mean error
    # cnot_properties = backend.properties().gate_property(cnot_in_backend)
    #mean_cnot_error = np.array([ backend.properties().gate_property('cz')[pair]['gate_error'][0] for pair in backend.properties().gate_property('cz').keys() ]).mean()
    #mean_cnot_error = np.array([ cnot_properties[pair]['gate_error'][0] for pair in cnot_properties.keys() ]).mean()
    #TODO: check for qubit pairs for ibm_backend (EAGLE)
    mean_cnot_error = np.array([ cnot_properties[pair]['gate_error'][0] for pair in qubitpairs ]).mean()
    # 4. Get errors for the paired qubits
    tolerance = 1.0
    faulty_pairs = [ pair for pair in qubitpairs if cnot_properties[pair]['gate_error'][0] >= (1. + tolerance) * mean_cnot_error ]
    if len(faulty_pairs) > 0:
        print('')
        print('******************************************************************')
        print('WARNING: found possible FAULTY CNOT gate or with CALIBRATION ERROR')
        print('         Faulty pairs with gate error large than {}% '.format(tolerance*100))
        print('         of the mean error {} :'.format(mean_cnot_error))
        for faulty_pair  in faulty_pairs: 
            print('{} - gate error: {}'.format(faulty_pair, cnot_properties[faulty_pair]['gate_error'][0]))
        print('******************************************************************')
        print('')

    

    #print([qc._data[i].name for i in range(len(qc._data))])
    #print('Computing gate error')
    for i, item in enumerate(qc._data):
        gate = item.operation.name
        q0 = qc.find_bit(item.qubits[0]).index
        idx_q0 = error.qubit_list.index(q0)
        if gate in backend.operation_names:
            if gate in cnot_names: #(gate == 'cx') or (gate == 'ecr') or (gate == 'cz'):
                q1 = qc.find_bit(item.qubits[1]).index
                idx_q1 = error.qubit_list.index(q1)

                gatememory[idx_q0][i] = 1
                gatememory[idx_q1][i] = 1

                #adding gates due to control gate into target gate
                # modifing list by adding 1 of control... # TODO: check if this is correct or we have to check by layer.
                gatememory[idx_q1] = [max(a, b) for a, b in zip(gatememory[idx_q0], gatememory[idx_q1])]
                
                pair = (q0,q1)
                if pair not in cnot_properties.keys():
                    pair = pair[::-1]
                gateerror[i]=backend.target[gate][pair].error

                #gate_time = props.gate_length(gate, [q0, q1])
                #gate_time = gatetimes.get(gate, [q0, q1], unit='s')
                gate_time = gatetimes.get(gate, pair, unit='s')
                time[idx_q0] += gate_time
                time[idx_q1] += gate_time
                time[idx_q0] = max([time[idx_q0], time[idx_q1]])
                time[idx_q1] = max([time[idx_q0], time[idx_q1]])
                #cnot_error = backend.target[gate][(q0, q1)].error
                cnot_error = backend.target[gate][pair].error
                fidel_cnot *= (1 - cnot_error)
           #     fidel_gate[idx_q0] *= (1 - cnot_error)
           #     fidel_gate[idx_q1] *= (1 - cnot_error)

           # compute new commulative error from entangled/control qubits
           # We understand : q0 = control , q1 = target
                #fidel_time[idx_q0] = np.exp(-time[idx_q0]/tdecay[idx_q0][0])*np.exp(-time[idx_q0]/tdecay[idx_q0][1])
                #fidel_time[idx_q1] = np.exp(-time[idx_q1]/tdecay[idx_q1][0])*np.exp(-time[idx_q1]/tdecay[idx_q1][1])

                #fidel_qb[idx_q0] = fidel_gate[idx_q0]*fidel_time[idx_q0]

                # add error from control qubit
                #fidel_gate[idx_q1] *=  fidel_qb[idx_q0]
                #fidel_qb[idx_q1] = fidel_gate[idx_q1]*fidel_time[idx_q1]

            elif gate == 'measure':
                gatememory[idx_q0][i] = 1
                #print('Measure found')
                gate_time = gatetimes.get(gate, q0, unit='s')
                time[idx_q0] += gate_time

                gateerror[i] = backend.target[gate][(q0, )].error
                measurement_error = backend.target[gate][(q0, )].error
                fidel_measurement *= 1 - measurement_error
            #    fidel_time[idx_q0] = np.exp(-time[idx_q0]/tdecay[idx_q0][0])*np.exp(-time[idx_q0]/tdecay[idx_q0][1])
            #    fidel_qb[idx_q0] = fidel_measurement * fidel_gate[idx_q0] * fidel_time[idx_q0]
            else:
                if gate != 'barrier':
                    gatememory[idx_q0][i] = 1
                    #gate_time = props.gate_length(gate, q0)
                    gate_time = gatetimes.get(gate, q0, unit='s')
                    time[idx_q0] += gate_time
                    sgate_error = backend.target[gate][(q0, )].error

                    gateerror[i] = backend.target[gate][(q0, )].error
                    fidel_sgate *= (1 - sgate_error)
                    #fidel_gate[idx_q0] *= (1 - sgate_error)

                    #fidel_time[idx_q0] = np.exp(-time[idx_q0]/tdecay[idx_q0][0])*np.exp(-time[idx_q0]/tdecay[idx_q0][1])
                    #fidel_qb[idx_q0] = fidel_gate[idx_q0]*fidel_time[idx_q0]

    # Compute fidel_qb from from gatememory
    #print(len(gateerror))
    #print(len(gatememory[0]))
    #print(len(gatememory[1]))
    fidel_gate =  [ np.prod(1-np.array(gateerror)[memory == 1]) for memory in np.array(gatememory) ] 



    #TODO: add time error due to the fact that qc in ESTIMATORS has not measures at the end.
    #      1. check if the last gate in qc._data is a measure gate
    if item.operation.name != 'measure' :
#   #      2. if not, add time error due the measure for all qubits .
        for q0 in  qc.layout.final_index_layout() :
            gate_time = gatetimes.get('measure', q0, unit='s')
            idx_q0 = error.qubit_list.index(q0)
            time[idx_q0] += gate_time
            fidel_time[idx_q0] = np.exp(-time[idx_q0]/tdecay[idx_q0][0])*np.exp(-time[idx_q0]/tdecay[idx_q0][1])
            fidel_qb[idx_q0] =  fidel_gate[idx_q0]*fidel_time[idx_q0]
#            print(q0, gate_time, fidel_time[idx_q0], fidel_gate[idx_q0] ,  fidel_gate[idx_q0]*fidel_time[idx_q0])
    else :
        for q0 in  qc.layout.final_index_layout() :
            idx_q0 = error.qubit_list.index(q0)
            fidel_time[idx_q0] = np.exp(-time[idx_q0]/tdecay[idx_q0][0])*np.exp(-time[idx_q0]/tdecay[idx_q0][1])
            fidel_qb[idx_q0] =  fidel_gate[idx_q0]*fidel_time[idx_q0]

    


    
#   
#    circuit_time = max(time)
#    #print('Computing time error')
#    for qubit in error.qubit_list:
#        #Time errors
#        qubit_idx = error.qubit_list.index(qubit)
#        t1 = backend.qubit_properties(qubit).t1
#        t2 = backend.qubit_properties(qubit).t2
#        #print(qubit, t1, t2)
#        #time_error = qubit_time_error(circuit_time, t1s[qubit], t2s[qubit])
#        #fidel_time *= 1 - time_error
#        #fidel_qb[qubit_idx] *= (1 - time_error)
#        ftime = np.exp(-circuit_time/t1)*np.exp(-circuit_time/t2)
#        fidel_time *= ftime
#        fidel_qb[qubit_idx] *= ftime 

    #print('num_qubits=',len(error.qubit_list))
    #for q in range(len(error.qubit_list)) :
    #    print(q, error.qubit_list[q], gatememory[q])
        #print(q, gatememory[q])

    error.cx = 1 - fidel_cnot
    error.sgate = 1 - fidel_sgate
    error.time = 1 - (np.prod(fidel_time))
    error.measurement = 1 - fidel_measurement
    fidel_total = fidel_cnot * fidel_sgate * np.prod(fidel_time) * fidel_measurement
    error.total = 1 - fidel_total
    #print('Fidel_qb : {}'.format(fidel_qb))
    error.total_qubits = [float(1 - fidel) for fidel in fidel_qb]
    return error





if __name__ == '__main__':
    print('Please  run some of the tests in test/')

