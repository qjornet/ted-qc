#    Copyright (C) 2023 J. Jornet-Somoza, N. Sobrino, J. Borge, G. Sobrino, U. Aseguinolaza

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from qiskit import QuantumCircuit
#from qiskit.transpiler.preset_manager import generate_preset_pass_manager
from qiskit import transpile
from qiskit_ibm_runtime import SamplerV2 as Sampler
from qiskit_ibm_runtime import EstimatorV2 as Estimator
from qiskit.quantum_info import SparsePauliOp
from qiskit_aer import AerSimulator

from qiskit_ibm_runtime.fake_provider import FakeTorino

from ted_qc import *
import matplotlib.pyplot as plt
import time
#from qiskit.circuit.library import HGate, SdgGate
#from qiskit.circuit import ClassicalRegister


from qiskit_ibm_runtime import QiskitRuntimeService

service = QiskitRuntimeService(
    channel='ibm_quantum',
    instance='YOUR_INSTANCE_HERE',
    token='YOUR_TOKEN_HERE'
)
# Or save your credentials on disk.
# QiskitRuntimeService.save_account(channel='ibm_quantum', instance='YOUR_INSTANCE_HERE', token=''YOUR_TOKEN_HERE)


#backend = [service.least_busy()]
backend = [FakeTorino()]



class IsingModel:

    class Options(dict):
        def __init__(self, *args, **kwargs):
            super().__init__()
            self['nq'] = 4
            self['h'] = 1
            self['J'] = 0.15
            self['dt'] = 1/4
            self['depth'] = 15
            self.update(*args, **kwargs)

        def config_4q_paper(self):
            self['h'] = 1
            self['J'] = 0.15
            self['dt'] = 1/4*2

        def config_10q_paper(self):
            self['h'] = 1
            self['J'] = 0.5236
            self['dt'] = 1/4

    @classmethod
    def apply_quantum_circuit_layer(cls, qc: QuantumCircuit, ops: 'IsingModel.Options'):
        allq = list(range(ops['nq']))

        qc.rx(2*ops['h']*ops['dt'], allq)

        qc.barrier()
        for q0 in allq[0::2]:
            q1 = q0+1
            qc.cx(q0,q1)
        qc.rz(-2*ops['J']*ops['dt'], allq[1::2])
        for q0 in allq[0::2]:
            q1 = q0+1
            qc.cx(q0,q1)

        qc.barrier()
        for q0 in allq[1:-2:2]:
            q1 = q0+1
            qc.cx(q0,q1)
        qc.rz(-2*ops['J']*ops['dt'], allq[2:-1:2])
        for q0 in allq[1:-2:2]:
            q1 = q0+1
            qc.cx(q0,q1)
        qc.barrier()

    @classmethod
    def make_quantum_circuit(cls, ops: 'IsingModel.Options'):
        qc = QuantumCircuit(ops['nq'])
        allq = list(range(ops['nq']))

        for step in range(ops['depth']):
            cls.apply_quantum_circuit_layer(qc, ops)

        if ops['measure_basis'] == 'Z':
            pass
        elif  ops['measure_basis'] == 'X':
            qc.h(allq)
        elif  ops['measure_basis'] == 'Y':
            qc.sdg(allq)
            qc.h(allq)
        else:
            raise Exception("Must be X Y or Z")
        qc.measure_all()

        return qc

    @classmethod
    def make_circs_sweep(cls, ops, num_steps, measure_basis):
        ops['measure_basis'] = measure_basis
        ops['depth'] = num_steps
        qc = cls.make_quantum_circuit(ops)
        qc.metadata = {}
        qc.metadata['measure_basis'] = measure_basis
        qc.metadata['depth'] = num_steps
        return qc

ising = IsingModel()
ops = ising.Options({'nq':12,'depth':2, 'measure_basis':'Z'})
print(ops)
circuit = ising.make_quantum_circuit(ops)
#print(qc)

errors = []
circuits = []
layoutbackend = []
for bckend in backend:
    try:
        start = time.time()
        print('Transpiling')
        qc = transpile(circuit, backend=bckend, optimization_level=3)
        print('Circuit Transpiled')
        end = time.time()
        print('Tranpiling time: {} s'.format(end - start))
        print('QCircuit depth: {}; \# gates: {}'.format(qc.depth(), qc.size()))

        start = time.time()
        circuits.append(qc)
        errors.append(ibm_circuit_error(qc, backend=bckend))
        layoutbackend.append(bckend)
        print('Appending time: {} s'.format(time.time() - start))
    except:
        print('{}: This backend has no layout\n'.format(bckend))

# EXAMPLE FOR Sampler run --> get wfc amplitudes.
#    sampler = Sampler(mode = bckend)
#    job_results = sampler.run([qc]).result()
#    results = job_results[0].data.meas.get_counts()
#    from qiskit.visualization import plot_histogram
#    plot_histogram(job.result()[0].data.meas.get_counts())
#    plt.show()
     
# EXAMPLE FOR Estimator run --> get expectation values of an observable.
    estimator = Estimator(mode = bckend, options={"default_shots": 4096})
    observables = SparsePauliOp(['ZIII','IZII','IIZI','IIIZ'])
    obs = observables.apply_layout(layout=qc.layout)
    pub = [qc, obs]
    job = estimator.run([pub])
    print()
    #print('Eigenvalue of obs:  {} is {}'.format(observables, job.result()[0].data['evs']))
    print('Eigenvalue of obs:  {} is {}'.format(observables, job.result()[0].data.evs))



for i, error in enumerate(errors):
    print(' ')
    print('Backend: {}'.format(layoutbackend[i]))
    print('  total_error = {}, physical qubits = {} '.format(error.total, error.qubit_list))
    print('  CX_error = {} '.format(error.cx))
    print('  SGATE_error = {}'.format(error.sgate))
    print('  TIME_error = {} '.format(error.time))
    print('  MEASUREMENT_error = {}'.format(error.measurement))
