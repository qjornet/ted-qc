#    Copyright (C) 2023 J. Jornet-Somoza, N. Sobrino, J. Borge, G. Sobrino, U. Aseguinolaza

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from qiskit import QuantumCircuit,QuantumRegister
from qiskit import transpile
from ted_qc import *

#from qiskit import *
from qiskit.circuit.library import  ZGate


class grover_qb(object):
    def __init__(self, n, x):
        self.n=n
        self.x = x

    #encargado de marcar el elemento deseado 
    def Oraculo(self):
        qc = QuantumCircuit(self.n, name = 'Oraculo')
        x_bin = bin(self.x)[2:].zfill(self.n)

        for indx in range(self.n):
            if x_bin[self.n-1-indx] == '0':
                qc.x(indx) 

        if self.n == 1:
            qc.z(0)
        else:
            qc.append(ZGate().control(self.n-1), range(self.n))

        for indx in range(self.n):
            if x_bin[self.n-1-indx] == '0':
                qc.x(indx)

        return qc

    #encargado de realizar la inversión de fase sobre la media
    def Mirror(self):
        datos = QuantumRegister(self.n, name = 'q')
        qc=QuantumCircuit(datos, name = 'Mirror')

        qc.h(datos)
        qc.x(datos)

        if self.n == 1:
            qc.z(0)
        else:
            qc.append(ZGate().control(self.n-1), range(self.n))    
        
        qc.x(datos)
        qc.h(datos)
        
        return qc


    #n_itera calcula el número óptimo de iteraciones de grover para maximizar la probabilidad
    def n_itera(self):
        n_iter=(np.pi*2**(self.n/2 - 2))
        return int(n_iter)



    def grover_algorithm(self):
    
        # Sea n>1 el número de qubits, de modo que suponemos la lista de elementos L = [0, 1, ... 2^{n}-1]
        # Sea x el número natural que buscamos, de modo que nos lo devolverá en caso de que 0 <= x <= 2^{n} - 1
        
        n_iter= self.n_itera()

        qr = QuantumRegister(self.n)
        qc = QuantumCircuit(qr)

        qc.h(qr)
        
        for iteration in range(n_iter):
            qc.append(self.Oraculo(),range(self.n))
            qc.append(self.Mirror(), qr)

        #qc.measure(qr, output)
        return qc
    

    def create_grover_circuit(self): ## En el main
        '''Return the quantum circuit for the grover's algorithm'''
        # Create the circuit to run
        qc = QuantumCircuit(self.n, self.n)
  
        #qc.append(self.algoritmo_grover(),range(self.n))
        qc.append(self.grover_algorithm(),range(self.n))
        qc.measure(range(self.n),range(self.n))
        return qc


# IBMQ.save_account(TOKEN)
#IBMQ.load_account()
#credentials = {'hub':'ibm-q' , 'group':'open', 'project':'main'} 
#provider = IBMQ.get_provider(hub=credentials['hub'], group=credentials['group'], project=credentials['project'])
#backends = provider.backends()
#backend = backends


from qiskit_ibm_runtime import QiskitRuntimeService

service = QiskitRuntimeService(
    channel='ibm_quantum',
    instance='YOUR-INSTANCE-HERE',
    token='YOUR-TOKEN-HERE',
)

backend = [service.least_busy(min_num_qubits=127)]

# Circuit definition
grover = grover_qb(4,3)
circuit = grover.create_grover_circuit()

# Computing probability error
errors = []
circuits = []
layoutbackend = []
for bckend in backend:
    try:
        qc = transpile(circuit, backend=bckend, optimization_level=3)
        circuits.append(qc)
        errors.append(ibm_circuit_error(qc, backend=bckend))
        layoutbackend.append(bckend)
    except:
        print('{}: This backend has no layout\n'.format(bckend))

for i, error in enumerate(errors):
    print(' ')
    print('Backend: {}'.format(layoutbackend[i]))
    print('  total_error = {}, physical qubits = {} '.format(error.total, error.qubit_list))
    print('  CX_error = {} '.format(error.cx))
    print('  SGATE_error = {}'.format(error.sgate))
    print('  TIME_error = {} '.format(error.time))
    print('  MEASUREMENT_error = {}'.format(error.measurement))

