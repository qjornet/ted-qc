#    Copyright (C) 2023 J. Jornet-Somoza, N. Sobrino, J. Borge, G. Sobrino, U. Aseguinolaza

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from qiskit import QuantumCircuit
#from qiskit.transpiler.preset_manager import generate_preset_pass_manager
from qiskit import transpile
from ted_qc import *

## clase que implementa U_dagger

class ising_4qb(object):
    def __init__(self, n, landa):
        #TODO: check if the qc introduced corresponds to a 4 qubits circuit
        self.landa=landa
        self.n = n

    def FFT(self,k):
        qc = QuantumCircuit(2,name = 'FFT')
        qc.p(2*np.pi*k/self.n, 0)
        qc.cx(0,1)
        qc.ch(1,0)
        qc.cx(0,1)
        qc.cz(0,1)
        #qc.draw(output='mpl')
        gate = qc.to_gate()
        return gate

    def fSwap(self):
        qc = QuantumCircuit(2,name = 'fSwap')
        qc.swap(0,1)
        qc.cz(0,1)
        gate = qc.to_gate()
        return gate

    def Bogoliubov(self, k):
        qc = QuantumCircuit(2, name = 'Bog')
        qc.x(1)
        qc.cx(1,0)

        num = self.landa - np.cos(2*np.pi*k/self.n)
        den = np.sqrt((self.landa - np.cos(2*np.pi*k/self.n))**2 + np.sin(2*np.pi*k/self.n)**2)
        theta = np.arccos(num/den)

        qc.crx(theta, 0, 1)
        qc.cx(1,0)
        qc.x(1)
        gate = qc.to_gate()
        return gate

    def U_dg(self):### U hermitico, '_dg' es dagger
        qc = QuantumCircuit(4, name = 'U')
        qc.append(self.fSwap(),[1,2])
        qc.append(self.FFT(0), [0,1])
        qc.append(self.FFT(0), [2,3])
        qc.append(self.fSwap(),[1,2])
        qc.append(self.FFT(1), [0,1])
        qc.append(self.FFT(0), [2,3])
        qc.barrier()
        qc.append(self.Bogoliubov(1), [0,1])

        qc = qc.inverse()

        qc.barrier()

        #qc.measure(range(4),range(4))
        return qc 



from qiskit_ibm_runtime import QiskitRuntimeService

service = QiskitRuntimeService(
    channel='ibm_quantum',
    instance='YOUR_INSTANCE_HERE',
    token='YOUR_TOKEN_HERE'
)
# Or save your credentials on disk.
# QiskitRuntimeService.save_account(channel='ibm_quantum', instance='YOUR_INSTANCE_HERE', token=''YOUR_TOKEN_HERE)


backend = [service.least_busy()]

## parametros 
landa = 2.5
n = 4

# Circuit definition
circuit = QuantumCircuit(4,4)
# initial state
for i in np.arange(4):
    circuit.x(i)

# apply transformation for the ising hamiltonian
ising = ising_4qb(n,landa)
circuit.append(ising.U_dg(),[0,1,2,3])
circuit.measure(range(4),range(4))

errors = []
circuits = []
layoutbackend = []
for bckend in backend:
    try:
        qc = transpile(circuit, backend=bckend, optimization_level=3)
        circuits.append(qc)
        errors.append(ibm_circuit_error(qc, backend=bckend))
        layoutbackend.append(bckend)
    except:
        print('{}: This backend has no layout\n'.format(bckend))

for i, error in enumerate(errors):
    print(' ')
    print('Backend: {}'.format(layoutbackend[i]))
    print('  total_error = {}, physical qubits = {} '.format(error.total, error.qubit_list))
    print('  CX_error = {} '.format(error.cx))
    print('  SGATE_error = {}'.format(error.sgate))
    print('  TIME_error = {} '.format(error.time))
    print('  MEASUREMENT_error = {}'.format(error.measurement))

