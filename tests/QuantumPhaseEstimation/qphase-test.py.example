#    Copyright (C) 2023 U., J. Borge, J. Jornet-Somoza, N. Sobrino, G. Sobrino

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import numpy as np
from qiskit import QuantumCircuit
from qiskit import transpile
import matplotlib.pyplot as plt
from ted_qc import *

# some functions to define the circuit
def qft_dagger(qc, n):
    '''n-qubit QFTdagger the first n qubits in circ'''
    # Don't forget the Swaps!
    for qubit in range(n//2):
        qc.swap(qubit, n-qubit-1)
    for j in range(n):
        for m in range(j):
            qc.cp(-np.pi/float(2**(j-m)), m, j)
        qc.h(j)

def create_phase_estimation_phase_gate_circuit(angle, counting_qubits, plotFig = False):
    qc = QuantumCircuit(counting_qubits + 1, counting_qubits)
    qc.x(counting_qubits)
    for qubit in range(counting_qubits):
        qc.h(qubit)
    repetitions = 1
    for counting_qubit in range(counting_qubits):
        for i in range(repetitions):
            qc.cp(angle, counting_qubit, counting_qubits)
        repetitions *= 2
    qc.barrier()
    qft_dagger(qc, counting_qubits)
    qc.barrier()
    for n in range(counting_qubits):
        qc.measure(n, n)
    if plotFig:
        qc.draw(output = 'mpl')
        plt.show()

    return qc

## IBMQ.save_account(TOKEN)
#credentials = {'hub':'ibm-q' , 'group':'open', 'project':'main'} 
#IBMQ.load_account()
#provider = IBMQ.get_provider(hub=credentials['hub'], group=credentials['group'], project=credentials['project'])
#backends = provider.backends()

## Choose backend
##TODO: make available all possible backends in provider: backends
##backend = [ provider.get_backend('ibmq_montreal'), provider.get_backend('ibmq_toronto') ]
#backend = backends


from qiskit_ibm_runtime import QiskitRuntimeService

service = QiskitRuntimeService(
    channel='ibm_quantum',
    instance='YOUR-INSTANCE-HERE',
    token='YOUR-TOKE-HERE'
)

backend = [service.least_busy(min_num_qubits=127)]

# Compute phase estimation vs error
angle = 2*np.pi/3 # The result is 1/3
counting_qubits = 3
circuit = create_phase_estimation_phase_gate_circuit(angle, counting_qubits)

errors = []
circuits = []
layoutbackend = []
for bckend in backend:
    try:
        from qiskit import transpile
        qc = transpile(circuit, backend=bckend, optimization_level=3)

        errors.append(ibm_circuit_error(qc, backend=bckend))
        circuits.append(qc)
        layoutbackend.append(bckend)
    except:
        print('{}: This backend has no layout\n'.format(bckend))

for i, error in enumerate(errors):
    print(' ')
    print('Backend: {}'.format(layoutbackend[i]))
    print('  total_error = {}, physical qubits = {} '.format(error.total, error.qubit_list))
    print('  CX_error = {} '.format(error.cx))
    print('  SGATE_error = {}'.format(error.sgate))
    print('  TIME_error = {} '.format(error.time))
    print('  MEASUREMENT_error = {}'.format(error.measurement))

