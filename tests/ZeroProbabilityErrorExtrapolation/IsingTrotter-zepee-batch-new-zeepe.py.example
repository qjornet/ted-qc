#    Copyright (C) 2023 J. Jornet-Somoza, N. Sobrino, J. Borge, G. Sobrino, U. Aseguinolaza

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from qiskit import QuantumCircuit
#from qiskit.transpiler.preset_manager import generate_preset_pass_manager
from qiskit_ibm_runtime import Session, Batch
from qiskit import transpile
from qiskit_ibm_runtime import SamplerV2 as Sampler
from qiskit_ibm_runtime import EstimatorV2 as Estimator
from qiskit.quantum_info import SparsePauliOp
from qiskit_aer import AerSimulator

from qiskit_ibm_runtime.fake_provider import FakeTorino, FakeSherbrooke

from ted_qc import *
import matplotlib.pyplot as plt
import time
import itertools

from datetime import datetime
import pickle
import os

from qiskit_ibm_runtime import QiskitRuntimeService

service = QiskitRuntimeService(
    channel='ibm_quantum',
    instance='YOUR_INSTANCE_HERE',
    token='YOUR_TOKEN_HERE'
)

# Or save your credentials on disk.
# QiskitRuntimeService.save_account(channel='ibm_quantum', instance='YOUR_INSTANCE_HERE', token=''YOUR_TOKEN_HERE)


backend = [service.least_busy(min_num_qubits=133)]
#backend = [FakeTorino()]
print(backend[0].name)


# Defining INPUT VARIABLES
num_qubits = 12
num_shots = 4096
J=np.pi
h=0
depth=5
measure_basis='Z'
#Define ZEPEE options
#factor = 4
factor = [1, 2, 3, 4, 5]

zepee_style = 'parallel'  # accepts serial or parallel


##### ----------------------------------------------- ###

def generate_pauli_z_sequence(num_qubits):
    sequence = []
    for i in range(num_qubits):
        # Create a string of 'I' for all qubits
        qubits = ['I'] * num_qubits
        # Set the ith qubit to 'Z'
        qubits[i] = 'Z'
        # Join the list into a string and add to sequence
        sequence.append(''.join(qubits))
    return sequence



class IsingModel:

    class Options(dict):
        def __init__(self, *args, **kwargs):
            super().__init__()
            self['nq'] = 4
            self['h'] = 1
            self['J'] = 0.15
            self['dt'] = 1/4
            self['depth'] = 15
            self.update(*args, **kwargs)

        def config_4q_paper(self):
            self['h'] = 1
            self['J'] = 0.15
            self['dt'] = 1/4*2

        def config_10q_paper(self):
            self['h'] = 1
            self['J'] = 0.5236
            self['dt'] = 1/4

    @classmethod
    def apply_quantum_circuit_layer(cls, qc: QuantumCircuit, ops: 'IsingModel.Options'):
        allq = list(range(ops['nq']))

        qc.rx(2*ops['h']*ops['dt'], allq)

        qc.barrier()
        for q0 in allq[0::2]:
            q1 = q0+1
            qc.cx(q0,q1)
        qc.rz(-2*ops['J']*ops['dt'], allq[1::2])
        for q0 in allq[0::2]:
            q1 = q0+1
            qc.cx(q0,q1)

        qc.barrier()
        for q0 in allq[1:-2:2]:
            q1 = q0+1
            qc.cx(q0,q1)
        qc.rz(-2*ops['J']*ops['dt'], allq[2:-1:2])
        for q0 in allq[1:-2:2]:
            q1 = q0+1
            qc.cx(q0,q1)
        qc.barrier()

    @classmethod
    def make_quantum_circuit(cls, ops: 'IsingModel.Options'):
        qc = QuantumCircuit(ops['nq'])
        allq = list(range(ops['nq']))

        for step in range(ops['depth']):
            cls.apply_quantum_circuit_layer(qc, ops)

        if ops['measure_basis'] == 'Z':
            pass
        elif  ops['measure_basis'] == 'X':
            qc.h(allq)
        elif  ops['measure_basis'] == 'Y':
            qc.sdg(allq)
            qc.h(allq)
        else:
            raise Exception("Must be X Y or Z")
#        qc.measure_all()

        return qc

    @classmethod
    def make_circs_sweep(cls, ops, num_steps, measure_basis):
        ops['measure_basis'] = measure_basis
        ops['depth'] = num_steps
        qc = cls.make_quantum_circuit(ops)
        qc.metadata = {}
        qc.metadata['measure_basis'] = measure_basis
        qc.metadata['depth'] = num_steps
        return qc


# Initializing Ising Options
ising = IsingModel() 
ops = ising.Options({'nq':num_qubits,'J':J, 'h':h, 'depth':depth, 'measure_basis':measure_basis})
print(ops)

# modifying range factor in case factor is an integer
if type(factor) == int:
    factor=range(factor+1)[1::]

# Initializing lists
circuit = ising.make_quantum_circuit(ops)

# Defining observable
pauli_op = generate_pauli_z_sequence(num_qubits)
print(pauli_op)
observables = SparsePauliOp(pauli_op)

errors = []
evs = []
circuits = []
layoutbackend = []
tedqcs = []
jobs = []

# Computing AER SIMULATOR VALUES
from qiskit_aer import AerSimulator
aer = AerSimulator()
jobsim = Estimator(mode = aer ,  options={"default_shots": num_shots} ).run([[circuit, observables]])
errors.append(0)
evs.append(jobsim.result()[0].data.evs)



for bckend in backend:
#try:
    print(bckend.name)
    start = time.time()
    print('Transpiling')
    qc = transpile(circuit, backend=bckend, optimization_level=3)
    print('Circuit Transpiled')
    end = time.time()
    print('Tranpiling time: {} s'.format(end - start))
    print('QCircuit depth: {}; \# gates: {}'.format(qc.depth(), qc.size()))
    circuits.append(qc.copy())
  
    # Transpiling observables                                                                       
    obs = observables.apply_layout(layout=qc.layout)

    # Adding error 0 to zne calculation
    errors.append(0)

    tedqc = ibm_circuit_error(qc, backend=bckend)
    tedqcs.append(tedqc)
    print('TED-qc total_qubits: {}'.format(tedqc.total_qubits))
    errors.append(np.array(tedqc.total_qubits).mean())

    # Preparing circuits for BATCH submission
 
    # find which 2 gate operations is implemented in the backend
    cnot = next((gate for gate in bckend.operations if gate.name in ['cx', 'ecr', 'cz']), None)
 
    # adding cnot pairs in transpilet circuit. 2 cnots or 
    last=0
    for f in factor:
        print()
        print('ZEEPE step {}'.format(f))
        for j in range(f-last):
            if zepee_style == 'serial' :
               for qubits in list(itertools.combinations(qc.layout.final_index_layout(), 2)):
                   try: 
                       if bckend.instruction_durations.get(cnot, list(qubits), unit='s') != 0:  
                           #print(cnot, list(qubits))
                           qc.append(cnot, qargs=list(qubits))
                           qc.append(cnot, qargs=list(qubits))
                           #qc.barrier(list(qubits))
                   except:
                       continue
                       #print('Qubits {} not connected '.format(qubits))
         
            elif zepee_style == 'parallel' :
                for q0 in qc.layout.final_index_layout()[0::2]:
                    n = qc.layout.final_index_layout().index(q0)
                    q1 = qc.layout.final_index_layout()[n+1]
                    qc.append(cnot, qargs=[q0, q1]) 
                    qc.append(cnot, qargs=[q0, q1]) 
                    #qc.append(cnot, qargs=[q1, q0]) 
                    #qc.append(cnot, qargs=[q1, q0]) 
         
                for q0 in qc.layout.final_index_layout()[1:-2:2]:
                    n = qc.layout.final_index_layout().index(q0)
                    q1 = qc.layout.final_index_layout()[n+1]
                    qc.append(cnot, qargs=[q0, q1]) 
                    qc.append(cnot, qargs=[q0, q1]) 
                    #qc.append(cnot, qargs=[q1, q0]) 
                    #qc.append(cnot, qargs=[q1, q0]) 
       
        last = f

        circuits.append(qc.copy())
        tedqc = ibm_circuit_error(qc, backend=bckend)
        tedqcs.append(tedqc)
        print('TED-qc error: total {}, mean qubit {}'.format(tedqc.total, np.array(tedqc.total_qubits).mean()))
        errors.append(np.array(tedqc.total_qubits).mean())

    for i, qc in enumerate(circuits):
        #print(qc.draw('text', idle_wires=False))
        if i == 0: 
            print('qc factor: {} - Depth: {} '.format(i, qc.depth(lambda instr: len(instr.qubits) == 2) ))
            print('qc factor: {} - size: {} '.format(i, qc.size(lambda instr: len(instr.qubits) == 2) ))
        else:
            print('qc factor: {} - Depth: {} '.format(factor[i-1], qc.depth(lambda instr: len(instr.qubits) == 2) ))
            print('qc factor: {} - size: {} '.format(factor[i-1], qc.size(lambda instr: len(instr.qubits) == 2) ))


    # ZNE reference
    pub = [circuits[0], obs]
    estimator = Estimator(mode = bckend, options={"default_shots": num_shots, "resilience_level":2,   "resilience": {"zne_mitigation": True, "zne": {"noise_factors": [1, 3, 5], "extrapolator":'linear'}}} )
    jobzne = estimator.run([pub])
    evs.append(jobzne.result()[0].data.evs)

    # Performing ZEEPE
    with Batch(backend=bckend) as batch: 
        estimator = Estimator(mode=batch, options={"default_shots": num_shots, "resilience_level":1, "twirling":{'enable_gates':True}})
        for i, qcirc in enumerate(circuits):
            pub = [qcirc, obs]
            #Run the ZEEPE circuits (
            # Defining Estimator
            job = estimator.run([pub])
            # Appending errors and evs
            evs.append(job.result()[0].data.evs)
 
 
    # evs is a list of array --> fatten
    evs = np.array(evs).flatten()
    errors = np.array(errors)
    for i, ev in enumerate(evs):
        print(i, errors[i], evs[i])
 
 
    from scipy.optimize import curve_fit
    def func(x, a, b ):
    #return a * np.exp(-b * x) + c
        return a*x + b
 
    #print(errors, evs)
    popt, pcov = curve_fit(func, errors[2:], evs[2:])
 
    print('X', 0, func(0,*popt))
    
    # Plotting results
    x = np.arange(0,max(errors)+0.1*max(errors),0.01)
    y = func(x, *popt)
    plt.plot(errors[0], evs[0], "*", label='AER')
    plt.plot(errors[1], evs[1], "v", label='zne')
    plt.plot(errors[2:], evs[2:], "o", label='TED-qc')
    plt.plot(x, y, "-", label='linear fit')
    plt.plot(0, func(0,*popt), 'X', label='zepee')
    plt.legend()
    plt.xlabel('TED-qc error probability')
    plt.ylabel('Expectation Value')
    plt.title('Backend: {}\nIsing ops: {}'.format(bckend.name, ops ))
            
    plt.savefig('zepee-{}.png'.format(bckend.name))
    plt.show()



current_datetime = datetime.now()
formatted_datetime = current_datetime.strftime("%Y-%m-%dT%Hh%Mm%Ss")
print("Formatted date and time:", formatted_datetime)

pwd = os.getcwd()
resfolder = pwd+'/results/'
if not os.path.exists(resfolder):
    os.mkdir(resfolder)

file = open(resfolder + '{}-errors_{}-nq_{}-d_{}-J_{}-h_{}-factor{}.pkl'.format(formatted_datetime, backend[0].name, ops['nq'], ops['depth'], ops['J'], ops['h'], factor), 'wb')
pickle.dump(errors, file)
file.close()
file = open(resfolder + '{}-values_{}-nq_{}-d_{}-J_{}-h_{}-factor{}.pkl'.format(formatted_datetime, backend[0].name, ops['nq'], ops['depth'], ops['J'], ops['h'], factor), 'wb')
pickle.dump(evs, file)
file.close()
file = open(resfolder + '{}-tedqc_{}-nq_{}-d_{}-J_{}-h_{}-factor{}.pkl'.format(formatted_datetime, backend[0].name, ops['nq'], ops['depth'], ops['J'], ops['h'], factor), 'wb')
pickle.dump(tedqcs, file)
file.close()

print()
#print('TED-qc  errors by source:')
##print(''.format('Label',''))
#for i, tedqc in enumerate(tedqcs):
#    print('{} '.format(i))
#    print(", ".join(f"{key}: {value}" for key, value in tedqc.__dict__.items()))

#except:
#    print('{}: This backend has no layout\n'.format(bckend))

# EXAMPLE FOR Sampler run --> get wfc amplitudes.
#    sampler = Sampler(mode = bckend)
#    job_results = sampler.run([qc]).result()
#    results = job_results[0].data.meas.get_counts()
#    from qiskit.visualization import plot_histogram
#    plot_histogram(job.result()[0].data.meas.get_counts())
#    plt.show()
     
# EXAMPLE FOR Estimator run --> get expectation values of an observable.
#    estimator = Estimator(mode = bckend, options={"default_shots": 4096})
#    observables = SparsePauliOp(['ZIII','IZII','IIZI','IIIZ'])
#    obs = observables.apply_layout(layout=qc.layout)
#    pub = [qc, obs]
#    job = estimator.run([pub])
#    print()
#    #print('Eigenvalue of obs:  {} is {}'.format(observables, job.result()[0].data['evs']))
#    print('Eigenvalue of obs:  {} is {}'.format(observables, job.result()[0].data.evs))
#
#
#
##for i, error in enumerate(errors):
#    print(' ')
#    print('Backend: {}'.format(layoutbackend[i]))
#    print('  total_error = {}, physical qubits = {} '.format(error.total, error.qubit_list))
#    print('  CX_error = {} '.format(error.cx))
#    print('  SGATE_error = {}'.format(error.sgate))
#    print('  TIME_error = {} '.format(error.time))
#    print('  MEASUREMENT_error = {}'.format(error.measurement))
