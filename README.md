## TED-qc : Tool for Error Description in Quantum Circuits 

## Description
TED-qc tool enables IBM Quantum Computer users to find the a lower-bound of the total error for a given quantum circuit.

![ted-qc scheme](images/tedqc-scheme.png)

## Installation

1) Download or clone from the git repository.

git clone https://gitlab.com/qjornet/ted-qc.git

2) Include the full path of the library location in your \$PYTHONPATH:
export PYTHONPATH=\$INSTALLPATH/ted-qc:\$PYTHONPATH

3) In order to access to the IBM Quantum Computer API, you must properly load your provider

from qiskit import IBMQ
IBMQ.load_account()
credentials = {'hub':'ibm-q' , 'group':'open', 'project':'main'}
provider = IBMQ.get_provider(hub=credentials['hub'], group=credentials['group'], project=credentials['project'])
backends = provider.backends()
backend = backends

## Usage
You can find 3 examples on how to use the TED-qc in the tests folder:
1) Diagonalization of the Ising model
2) Quantum Phase Estimation
3) Grover's algorithm

## Support
If you have any question or suggestion, please contact with: joaquim.jornet@ehu.eus, n


## License
This software is distributed under a GPL license.






## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/qjornet/ted-qc.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://gitlab.com/qjornet/playing-with-qcomputers/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:5476df069fe31c3ccaba5078de57b760?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

